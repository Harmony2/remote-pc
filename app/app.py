#!/bin/python3

from flask import Flask, session, render_template, request, redirect
import os
from pynput.keyboard import Key, Controller
keyboard = Controller()
import pprint
import time

app = Flask(__name__)

class Volume:
    def __init__(self):
        output = os.popen("amixer get Master").read().split("[")
        for i in output:
            if "%" in i:
                splitted = i
        splitted = splitted.replace("%", "")
        self.sound_volume = int(splitted.replace("]", ""))


    def volume_up(self):
        self.sound_volume += 5
        os.system("amixer sset Master %d%%"%self.sound_volume)


    def volume_down(self):
        self.sound_volume -= 5
        os.system("amixer sset Master %d%%"%self.sound_volume)


    def volume_mute(self):
        self.sound_volume = 0
        os.system("amixer sset Master %d%%"%self.sound_volume)

sound = Volume()


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/downv")
def volume_down():
    keyboard.press(Key.down)
    keyboard.release(Key.down)
    return render_template("index.html")


@app.route("/upv")
def volume_up():
    keyboard.press(Key.up)
    keyboard.release(Key.up)
    return render_template("index.html")


@app.route("/mutev")
def volume_mute():
    keyboard.press(Key.down)
    time.sleep(1.1)
    keyboard.release(Key.down)
    return render_template("index.html")


@app.route("/shutdownnow")
def shutdown_now():
    os.system("shutdown -P +1")
    return render_template("index.html")


@app.route("/shutdown1h")
def volume_1h():
    os.system("shutdown -P +60")
    return render_template("index.html")


@app.route("/shutdowncancel")
def shutdown_cancel():
    os.system("shutdown -c")
    return render_template("index.html")


@app.route("/rightarrow")
def right_arrow():
    keyboard.press(Key.right)
    keyboard.release(Key.right)
    return render_template("index.html")


@app.route("/leftarrow")
def left_arrow():
    keyboard.press(Key.left)
    keyboard.release(Key.left)
    return render_template("index.html")


@app.route("/space")
def space_bar():
    keyboard.press(Key.space)
    keyboard.release(Key.space)
    return render_template("index.html")


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')